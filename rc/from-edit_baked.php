<?php
include('v2/config.php');
include('v2/lib/utils.php');
include('v2/timecrumb.class.php');
include('v2/timebakery.class.php');
include('v2/annotation.class.php');

$input=json_encode($_POST);
$str_dec=json_decode($input);
$annotation_id=$str_dec->annotation_id;
$tb=new timebakery($annotation_id);
$tb->set_srt($str_dec->source);
$tb->parse_crumbs();
$tb->regenerate_srt();
$str_dec->source=$tb->get_srt();
$str_enc=json_encode($str_dec);
$an=new annotation($annotation_id);
$an->set_json_source($str_enc);
$an->export_json();
echo $input;
?>
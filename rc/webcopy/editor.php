<?php
include('../v2/config.php');
include('../v2/lib/utils.php');
include('../v2/timecrumb.class.php');
include('../v2/timebakery.class.php');
include('../v2/annotation.class.php');
$data_created='00:00:00';
$data_last_changed='00:00:00';
$id_start=$_GET['a'];
/* $id_start='0753438001377860937'; */
$an=new annotation($id_start);
$an->load_from_json();
$srt=$an->get_source();
$tb=new timebakery($id_start);
$tb->set_srt($srt);
$tb->parse_crumbs(false);
$crumbs=$tb->get_timecrumbs();
$an->set_timecrumbs($crumbs);
$str_playlist=$an->generate_editor_html();
$str_tmpl=file_get_contents('editor.tmpl.html');
$str_tmpl=preg_replace('/<!---aa-tmpl:title-->/',$an->get_resource(),$str_tmpl);
$str_tmpl=preg_replace('/<!---aa-tmpl:resource-->/',$an->get_resource(),$str_tmpl);
$str_tmpl=preg_replace('/<!---aa-tmpl:created-->/',$data_created,$str_tmpl);
$str_tmpl=preg_replace('/<!---aa-tmpl:last_changed-->/',$data_last_changed,$str_tmpl);
$str_tmpl=preg_replace('/<!---aa-tmpl:playlist-->/',$str_playlist,$str_tmpl);
print $str_tmpl;
?>
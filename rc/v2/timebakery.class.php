<?php
class timebakery{
  private $id,$srt,$lines,$lcodes,$timecrumbs,$header;
  const PAT='#([0-9]{2}:[0-9]{2})(:[0-9]{2})?(,)?([0-9]{1,3})? -->(\s)?([0-9]{2}:[0-9]{2})?(:[0-9]{2})?(,)?([0-9]{1,3})?#';
  const PATNORM='#([0-9]{2})?(:[0-9]{2})?(:[0-9]{2})?(,)?([0-9]{1,3})? -->(\s)?([0-9]{2})?(:[0-9]{2})?(:[0-9]{2})?(,)?([0-9]{1,3})?#';
  public function __construct($id){
    $this->id=$id;
  }
  public function get_id(){
    return $this->id;
}
  public function set_srt($srt){
    $this->srt=$srt;
  }
  public function get_srt(){
    return $this->srt;
  }
  public function get_timecrumbs(){
    return $this->timecrumbs;
}
  public function parse_crumbs($full=true){
    $this->set_lines();
    $this->index_lcodes();
    $this->set_timecrumbs();
    /* debug_r($this->lcodes); */
    /* debug_r($this->lines); */
    /* $this->sort_crumbs(); */
    if($full){
      $this->linearize_crumbs();
      $this->merge_crumbs();
    }
    debug_r($this->timecrumbs);

  }
  public function set_lines(){
    $this->lines=preg_split('/\n/',trim($this->srt));
  }
  public function index_lcodes(){
    $this->lcodes=array();
    for($i=0;$i<count($this->lines);$i++){
      $res = preg_match(self::PAT, $this->lines[$i], $matches);
      /* debug_r($matches); */
      if(sizeof($matches)>0){
	$this->lcodes[]=$i;
      }
    }
  }

  public function set_timecrumbs(){
    $this->timecrumbs=array();
    $tmp=array();
    $cur_crumb=false;
    $str_source='';
    $cnt=0;
    $cues=array();
    $timecode_started=null;
    for($i=0;$i<count($this->lines);$i++){
      if(in_array($i,$this->lcodes)){
	$timecode_started=true;
	if($cur_crumb){
	  $cur_crumb->set_source($str_source);
	  $idx=tc_to_int($cur_crumb->get_start_time());
	  //$idx allows to reorder later the array by start time
	  if(array_key_exists($idx,$tmp)){
	    $idx+=$i;
	  }
	  $tmp[$idx]=$cur_crumb;
	  $str_source='';
	}
	$cur_crumb=new timecrumb($cnt);
	$cues=$this->get_cues($this->lines[$i]);
	//debug_r($cues);
	$cues=tc_align($cues);
	debug_r($cues);
	$cur_crumb->set_start_time($cues['start_time']);
	$cur_crumb->set_end_time($cues['end_time']);
	$cnt++;

      }else{
	if(strlen(trim($this->lines[$i]))>0){
	  $cr='';
	  if(strlen($str_source)>0){
	    $cr="\n";
	  }
	  if(!$timecode_started){
	    $this->header.=$cr.trim($this->lines[$i]);
	  }else{
	    $str_source.=$cr.trim($this->lines[$i]);

	    if($i==(count($this->lines)-1)){
	      $cur_crumb->set_source($str_source);
	      $idx=tc_to_int($cur_crumb->get_start_time());
	      if(array_key_exists($idx,$tmp)){
		$idx+=$i;
	      }
	      $tmp[$idx]=$cur_crumb;


	    }}
	}
      }
    }
    ksort($tmp);
    foreach($tmp as $timecrumb){
      $this->timecrumbs[]=$timecrumb;
}
  }
  public function get_cues($str){
    $ret=array('start_time'=>null,'end_time'=>null);
    $res = preg_match(self::PATNORM, $str, $matches);
    //debug_r($matches);
    $ret['start_time']=$matches[1].$matches[2].$matches[3].$matches[4].$matches[5];
    if(sizeof($matches)>6){
      $ret['end_time']=$matches[7].$matches[8].$matches[9].$matches[10].$matches[11];
    }
    return $ret;
  }

  public function linearize_crumbs(){
    for($i=0;$i<count($this->timecrumbs);$i++){
      if($i<(count($this->timecrumbs)-1)){
	if($this->timecrumbs[$i]->get_end_time()==''){
	  //no end time, take next crumb start time
	  $this->timecrumbs[$i]->set_end_time($this->timecrumbs[($i+1)]->get_start_time());
	  debug($this->timecrumbs[$i]->get_source().'  : end time is null');
	}else if(tc_to_int($this->timecrumbs[$i]->get_end_time())>tc_to_int($this->timecrumbs[($i+1)]->get_start_time())){
	  //if end_time of timecrumb spans over next crumb start_time
	  $this->timecrumbs[$i]->set_end_time($this->timecrumbs[($i+1)]->get_start_time());
	  debug($this->timecrumbs[$i]->get_source().'  : end time is longer than next start time');
	}
  }}
}


  public function merge_crumbs(){
    $tmp=array();
    for($i=0;$i<count($this->timecrumbs);$i++){
      $index=tc_to_int($this->timecrumbs[$i]->get_start_time());
      if(array_key_exists($index,$tmp)){
	//crumbs with same start time need to b merged (or not?)
	//other option is to align those crumbs one after the other, but complex if more than two share the same start time
	$apd=$tmp[$index]->get_source()."\n".$this->timecrumbs[$i]->get_source();
	$tmp[$index]->set_source($apd);
	if($tmp[$index]->get_end_time()<$this->timecrumbs[$i]->get_end_time()){
	  $tmp[$index]->set_end_time($this->timecrumbs[$i]->get_end_time());
}
      }else{
	$tmp[$index]=$this->timecrumbs[$i];
      }
    }
    $this->timecrumbs=array();
    foreach($tmp as $crumb){
      $this->timecrumbs[]=$crumb;
    }
  }
  public function regenerate_srt(){
    $srt='';
    $srt.=$this->header."\n";
    for($i=0;$i<count($this->timecrumbs);$i++){
      $srt.=$this->timecrumbs[$i]->get_start_time()." -->";
      if(strlen($this->timecrumbs[$i]->get_end_time())>0){
	$srt.=" ".$this->timecrumbs[$i]->get_end_time();
}
      $srt.="\n";
      $srt.=$this->timecrumbs[$i]->get_source();
      $srt.="\n";

}
    $this->set_srt($srt);
}
  public function get_header(){
    return $this->header;
}
}
?>
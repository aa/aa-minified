<?php
include('v2/config.php');
include('v2/lib/utils.php');
include('v2/timecrumb.class.php');
include('v2/timebakery.class.php');
include('v2/annotation.class.php');
$id_start=$_GET['a'];
$an=new annotation($id_start);
$an->load_from_json();
$srt=$an->get_source();
$tb=new timebakery($id_start);
$tb->set_srt($srt);
$tb->parse_crumbs(false);
$crumbs=$tb->get_timecrumbs();
$an->set_annotation_header($tb->get_header());
$an->set_timecrumbs($crumbs);
$html=$an->generate_editor_html();
print $html;
?>
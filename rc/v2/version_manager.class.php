<?php
class version_manager{
  private $radic,$list,$ar_html_list,$res_url;
  function __construct($radic){
    $this->radic=$radic;
}

  public function lister(){
    $this->list=array();
    if ($handle = opendir(VERSIONPATH)) {
      while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
	  $expr='/^'.$this->radic.'/';
	  if(preg_match($expr,$entry)){
	    /* $entry=preg_replace('/\.txt$/','',$entry); */
	    $f=VERSIONPATH.'/'.$entry;
	    $this->list[filemtime($f)]=preg_replace('/\.txt$/','',$entry);
	  }}
      }
      closedir($handle);
    }
    ksort($this->list);
  }

  public function get_list(){
    return $this->list;
}
  public function html_list(){
    $this->ar_html_list=array();
    krsort($this->list);
    foreach($this->list as $k=>$v){
      $an=new annotation($v);
      $an->load_from_json(true);
      $srt=$an->get_source();
      $this->res_url=$an->get_resource();
      $this->ar_html_list[$v]='<div class="mdate">Modifiée le '.date ("d F Y H:i:s.", $k).'</div>'.$srt;
    }
    return $this->ar_html_list;
  }

  public function revert_to($str_in){
    if(preg_match('/__/',$str_in)){
      $pieces=explode("__",$str_in);
      if($pieces[0]==$this->radic){
	$f=$pieces[1];
	$an=new annotation($str_in);
	$an->load_from_json(true);
	$an_orig=new annotation($this->radic);
	$an_orig->load_from_json();
	$an_orig->set_json_source(json_encode($an->get_json_source()));
	$an_orig->export_json();
	$this->res_url=$an_orig->get_resource();
      }

    }
  }
  public function get_res_url(){
    return $this->res_url;
}
}

?>
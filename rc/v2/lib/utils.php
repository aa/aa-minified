<?php
function ser_save($name,$str_ser,$path='persist',$ext='.txt',$perms=false){
  $filename = $path.'/'.$name.$ext;
  if (!$handle = fopen($filename, 'w')) {
    echo "Cannot open file ($filename)";
    exit;
  }
  if (fwrite($handle, $str_ser) === FALSE) {
    echo "Cannot write to file ($filename)";
    exit;
  }
  if($perms){
    chmod($filename,0777);

}
  fclose($handle);
}
function ser_save_version($name,$str_ser,$path='persist',$ext='.txt'){
  $new_name=preg_replace("/(\.|\s)/","",microtime());
  $new_path='persist/versions';
  ser_save($new_name,$str_ser,$new_path,'.txt',true);
}

function ser_load($name,$path='persist',$ext='.txt'){
  $filename = $path.'/'.$name.$ext;
  $str_ser=file_get_contents($filename);
  return $str_ser;
}
function tc_to_int($tc){
  $ret=preg_replace('/[:,]/','',$tc);
  return $ret;
}

function tc_to_milli($tc){
  $secs='';
  $hours=0;
  if(strlen(trim($tc))==12){
    debug("an hour or more");
  }else{
    debug("less than an hour");
    $tc='00:'.$tc;
  }
  debug($tc);
  $hours = (int) substr($tc, 0, 2);
  $mins = (int) substr($tc, 3, 2) + $hours * 60;
  $secs = (int) substr($tc, 6, 2) + $mins * 60;
  $secs += ((int) substr($tc, 9, 3)) / 1000;
  return $secs;
}

function tc_align($cues){
  $ret=array();
  if(((tc_to_int($cues['start_time']))>(tc_to_int($cues['end_time'])))&&(strlen($cues['end_time'])>0)){
    $ret['start_time']=$cues['end_time'];
    $ret['end_time']=$cues['start_time'];
  }else{
    $ret=$cues;

  }
  return $ret;
}
function debug($str){
  if(DEBUG==1){
  print 'Debug: '.$str."\n";
  }
}
function debug_r($obj){
  if(DEBUG==1){
    print_r($obj);
  }
}

?>
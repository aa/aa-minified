<?php
include('v2/config.php');
include('v2/lib/utils.php');
include('v2/annotation.class.php');
include('v2/version_manager.class.php');
if(isset($_GET['a'])){
  $vm=new version_manager($_GET['a']);
  $vm->lister();
  $ar_versions_list=$vm->html_list();
  $backlink='<a href="webcopy/editor-plur.php?r='.$vm->get_res_url().'">Back to annotation</a>';

  $str_list='';
  $i=0;
  $tot=count($ar_versions_list);
  foreach($ar_versions_list as $k=>$v){
    if($i>0 && $i<($tot-1)){
      $revert="<div class=\"revert\"><a href=\"revert_to.php?to=".$k."\">Restaurer cette version.</a></div>";
      $link_raw="<div class=\"revert\"><a href=\"persist/versions/".$k.".txt\">Version brute</a></div>";
}
    else if($i==0){
      $revert="<div class=\"revert\">Version actuelle.</div>";

    }else{
      $revert='';
    }
    if(preg_match('/__/',$k)){
      $pieces=explode("__",$k);
      $a=$pieces[0];
      $ver=$pieces[1];
    }else{
      $a=$k;
      $ver=false;
}
    $v_title='file: '.$a.'<br />';
    if($ver){
    $v_title.='version: '.$ver;
}

    $str_list.='<div class="src_panel"><div class="version_header"><div class="version_title">'.$v_title.'</div>'.$revert.$link_raw.'</div>'.nl2br($v).'</div>';
    $i++;

  }}else{

  die('no reference passed');

}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Active Archives</title>
<style>
  body{
  font-size:0.9em;
}
  #container{
   width:786px;
}
.backlink{
  margin-bottom:20px;
}
.mdate{
   background:#ddd;
  border-bottom:1px solid #AAA;
   margin:0px 0 10px 0;
   padding-bottom:10px;
  padding-left:5px;

}
  .src_panel{
  border:1px solid black;
  float:left;
  font-family:monospace;
  font-size:11px;
  height:320px;
  margin:0 10px 10px 0;
  padding:5px;
  overflow:auto;
  width:240px;
}
.version_header{
 background:#ddd;
  padding-left:5px;

}
.version_title{
  /*
  background:rgb(30,30,30);
  color:white;
  */
  padding-top:5px;
}
</style>
</head>

<body>
<h1>Versions</h1>
<div class="backlink">
<?php
print $backlink;
?>
</div>
<div id="container">
<?php
  print $str_list;
?>
</div>
</body> </html>
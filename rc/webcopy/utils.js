function random_string(len)
{
    var text = "";
    var charset = "abcdefghijklmnopqrstuvwxyzQWERTYUIOPASDFGHJKLMNBVCXZ";
    for( var i=0; i < len; i++ )
        text += charset.charAt(Math.floor(Math.random() * charset.length));
    return text;
}
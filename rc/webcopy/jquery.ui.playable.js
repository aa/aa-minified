/**
 * jQuery UI Playable @VERSION
 *
 * Copyright 2011, AUTHORS.txt (http://activearchives.org/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 *
 * Depends:
 *   jquery.timecode.js
 *   jquery.ui.widget.js VERSION >=1.8!

 * A media tag abstraction that is like the HTML5 video tag
 * but with:
 * (1) support for different kinds of media (like mp4/flv via flowplayer), ogg (via video
 *     tag), and video sharing sites like youtube (& a normalized interface for each).
 * (2) start/end time support
 * 
 *  Events: play, pause, ended, timeupdate, durationchange

**/

// youtube's chromeless player API relies on some globally defined callbacks
aa_youtube_players_by_id = {}
function onYouTubePlayerReady(playerid) {
    var ytobj = aa_youtube_players_by_id[playerid];
    if (!ytobj) {
        console.log("error: youtube delegate not found for", playerid);
    }
    ytobj.youtubeReady();
}
function onYouTubePlayerStateChange (state, playerid) {
    // log("statechange", state);
    var ytobj = aa_youtube_players_by_id[playerid];
    if (!ytobj) {
        console.log("error: youtube delegate not found for", playerid);
    }
    ytobj.youtubeStateChange(state);    
}


(function ($, undefined) {

DEBUG = false;

function log () {
    try { console.log.apply(console, arguments); } catch (e) {}
}

$.widget("aa.playable", {
	options: {
        autoplay: false,
        autobuffer: false,
        autohide: true, // flowplayer
        controls: true,
        /* default == only if necessary -- ie for flowplayer, youtube, but not for <video> */
        default_width: 640,
        default_height: 360,
        default_audio_height: 24,
        fpScaling: 'fit',
        delegate: 'auto',
        default_delegate: 'html5',
        timeUpdateInterval: 1000 // determines frequency of timeupdate event callbacks (ms)
    },

    _create: function () {
	    // log("create", this.options);
        var o = this.options;
		this.element.removeClass("aa-playable");
        o._hasEnded = false;
        o._delegate = null;

        // start poller
        var that = this;
        o._pollintervalid = window.setInterval(function () { that._polltime() }, o._timeUpdateInterval);
    },

    _init: function () {
	    // log("init");

        var o = this.options;
        if (o.start !== undefined) {
            o._start = $.timecode_tosecs_attr(o.start);
            if (o.end) {
                o._end = $.timecode_tosecs_attr(o.end);
                // o._hasEnded = false; // this mustn't happen as it re-enables premature end detection on a clip restart
            }
        } else {
            o._start = null;
            o._end = null;
        }

        // log("init", o.src);
        var klass = this._getdelegate();
        if (o._klass && o._klass === klass && o._prevsrc === o.src) {
            // log("reusing old delegate");
        } else {
            o._klass = klass;
            if (o._delegate) {
                // log("destroying old delegate");
                o._delegate.destroy();
                o._delegate = null;
            }
            o._delegate = klass.init(this);
            o._prevsrc = o.src;
        }
        // log("klass", klass);
        // o._hasEnded = false;
        if (o._start !== null) this.seek(o._start);
        if (o.autoplay) this.play();
    },

    destroy: function () {
	    // log("destroy");
        var o = this.options;
		this.element.removeClass( "aa-playable" );

//		this.valueDiv.remove();

        // UNLOAD
        if (o._delegate) o._delegate.unload();
        if (o._pollintervalid) {
            window.clearInterval(o._pollintervalid);
            o._pollintervalid = null;
        }

		$.Widget.prototype.destroy.apply( this, arguments );
    },

    seek: function (t) {
        // log("playable, seek", t);
        var o = this.options;
        t = $.timecode_tosecs_attr(t);
        o._delegate.seek(t);
        /* now done in _polltime
        if (o._end !== null && o._hasEnded) {
            o._hasEnded = false;
        }
        */
    },

    toggle: function () {
        var o = this.options;
        this.getPaused() ? this.play() : this.pause();
    },
    
    play: function () {
        var o = this.options;
        if (o._delegate) {
            if (o._end !== null && o._hasEnded) {
                // log("media: restarting");
                this.seek(o._start);
                // o._hasEnded = false;
                // needs to happen somewhere else...
            }
            o._delegate.play();
        }
    },

    pause: function () {
        var o = this.options;
        if (o._delegate) o._delegate.pause();
    },

    getPaused: function () {
        var o = this.options;
        if (o._delegate) return o._delegate.getPaused();
    },

    getCurrentTime: function () {
        var o = this.options;
        if (o._delegate) return o._delegate.getCurrentTime();
    },

    getDuration: function () {
        var o = this.options;
        if (o._delegate) return o._delegate.getDuration();
    },

    _trigger: function (evt) {
        if (evt === "ended") {
            // console.log("playable: ended!");
            // trigger an actual DOM event on the element
            this.element.trigger("ended");
        }
        $.Widget.prototype._trigger.apply( this, arguments );
    },
    
    _getdelegate : function () {
        var o = this.options;
        // SELECT DELEGATE
        if (o.delegate) {
            o.delegate = o.delegate.toLowerCase();
            if (o.delegate == "auto") o.delegate = "";
        }
        
        if (o.delegate && delegatesByName[o.delegate]) {
            return delegatesByName[o.delegate];
        } else {
            for (var i=0; i<delegates.length; i++) {
                // guess the delegate by "SNIFF"ing the URL (VERY SIMPLE NOW)
                // console.log("sniff", o.src);
                if (delegates[i].sniff(o.src)) return delegates[i];
            }
        }
        // log("o", o);
        if (delegatesByName[o.default_delegate.toLowerCase()]) {
            // use default_delegate
            log("playable, falling back to default delegate", o.default_delegate);
            return delegatesByName[o.default_delegate.toLowerCase()];
        }
        log("playable: no delegate!");
    },

    _setdelegate : function () {
        var o = this.options;
        // SELECT / INSTANTIATE DELEGATE
        if (o.delegate) {
            o.delegate = o.delegate.toLowerCase();
            if (o.delegate == "auto") o.delegate = "";
        }
        
        if (o.delegate && delegatesByName[o.delegate]) {
            // attempt to use named delegate
            o._delegate = delegatesByName[o.delegate].init(this);
        } else {
            for (var i=0; i<delegates.length; i++) {
                // guess the delegate by "SNIFF"ing the URL (VERY SIMPLE NOW)
                if (delegates[i].sniff(o.src)) {
                    o._delegate = delegates[i].init(this);
                    break;
                }
            }
        }
        // log("o", o);
        if (!o._delegate && delegatesByName[o.default_delegate.toLowerCase()]) {
            // use default_delegate
            log("playable, falling back to default delegate", o.default_delegate);
            o._delegate = delegatesByName[o.default_delegate.toLowerCase()].init(this);
        }
        if (!o._delegate) { 
            log("playable: no delegate!");
            return;
        }
    },
    
    _polltime: function () {
        var o = this.options;
        // log("playable, polltime", o._end);
        var ct = null;
        ct = this.getCurrentTime();
        
        /* end time */
        if (o._end !== null && !o._hasEnded && ct >= o._end) {
            // log("playable: end detection");
            this.pause();
            o._hasEnded = true;
            this._trigger("ended");
        } else if (o._end !== null && o._hasEnded && ct < o._end) {
            // log("playable, restart detected");
            o._hasEnded = false;
            // this._trigger("play");
        }
       
        if (ct !== o._polltime_lasttime) { this._trigger("timeupdate", undefined, {time: ct}); }
        o._polltime_lasttime = ct;

        // try getting duration                
        if (!o._got_duration) {
            var cd = null;
            try {
                cd = this.getDuration();
            } catch (e) {}
            if (cd) {
                o._got_duration = true;
                this._trigger("durationchange");
            }
        }
    }

});



var delegates = [];
var delegatesByName = {};

function add_delegate(d) {
    delegates.push(d);
    delegatesByName[d.name] = d;
}

/*

Each delegate is a "closure class" implementing two functions, sniff & init.
Sniff takes a URL and returns true if the delegate wants to "claim" the URL.
Init takes the media object & options, initializes the media elt and returns a delegate closure.
The closure needs to implement:
    play, pause, getPaused, getCurrentTime, seek, getDuration, unload.
& should call:
    media.service_play, media.service_pause, media.service_ended
as appropriate (service_ended is only for the clip actually ending, not for a "virtual" end time -- the media object takes care of that case).

*/


/**********************************/
/* Flowplayer delegate            */
/**********************************/

add_delegate((function () {
    var klass = {};
    klass.name = "flowplayer";
    
    klass.sniff = function (url) {
        var urlpat = /\.(mp4|mp3|flv)/i;
        return urlpat.test(url);
    };
    
    klass.init = function (media) {
        var o = media.options;
        var that = {name: klass.name};
        
        // that.unload();
        var fpswf = $("link[rel=flowplayer]").attr('href');
        
        // this is less than ideal here
        if (o.audio === undefined) {
            var audiopat = /\.(mp3)$/i;
            o.audio = audiopat.test(o.src);
        }

        // log("flowplayer init", o.audio);

        var fpopts = {
            clip: {
                url: o.src,
                scaling: o.fpScaling,
                autoPlay: o.autoPlay,
            },
            plugins: {},
            wmode: "opaque"
        };
        if (!o.controls) {
            fpopts.plugins.controls = null;
        } else {
            if (o.autohide !== undefined) {
                fpopts.plugins.controls = {
                    // prevent autohide for audio (hack!)
                    autoHide: o.audio ? false : o.autohide
                }
            }
        }

        // create content element
        var fpelt = $("<div></div>").appendTo(media.element);
        that.fpelt = fpelt;
        
        var w = o.width || o.default_width;
        var h = o.height || (o.audio ? o.default_audio_height : o.default_height);
        $(fpelt).css({
            width: w + "px",
            height: h + "px"
        });
        fpopts.clip.autoPlay = o.autoplay;
        fpopts.clip.autoBuffering = o.autobuffer;
        
        // if (o.start) { fpopts.clip.start = o.start; }
        var onStartSeek = false;
        if (o._start) {
            // log("setting onStartSeek", o._start);
            onStartSeek = true;
        }
        if (!o.audio) {
            // install pseudostreaming support if possible
            // due to conflicts with the audio plugin (when seeking) -- do not use on audio
            var pseudostreaming = $("link[rel=flowplayerpseudostreaming]").attr('href');
            if (pseudostreaming) {
                fpopts.plugins['pseudo'] = { url: pseudostreaming };
                fpopts.clip.provider = 'pseudo';
            };
        }
        if (o.audio) {
            // install audioplugin if possible
            var audioplugin = $("link[rel=flowplayeraudio]").attr('href');
            if (audioplugin) {
                fpopts.plugins['audio'] = { url: audioplugin };
            };
        }
        fpopts.onLoad = function () {
            // this covers the little starting audio glitch when seeking, onStart will unmute
            this.mute();
        }
        
        // weird hack (for audio plugin, seems that onBegin is all that is called, against ver 3.2.5)
        fpopts.clip.onBegin = function () {
            if (o.audio) {
                // log("flowplayer, onBegin", this);
                if (onStartSeek) {
                    onStartSeek = false;
                    window.setTimeout(function () {
                        // log("seeking", media.start);
                        that.seek(o._start);
                    }, 0);
                }
                this.unmute();
                media._trigger("play");
            }
        };
        fpopts.clip.onStart = function () {
            // log("flowplayer, onStart", this);
            if (!o.audio) {
                if (onStartSeek) {
                    onStartSeek = false;
                    window.setTimeout(function () {
                        // log("seeking", o._start);
                        that.seek(o._start);
                    }, 0);
                }
                this.unmute();
                media._trigger("play");
            }
        };
        fpopts.clip.onResume = function () {
            // log("flowplayer.onResume");
            media._trigger("play");
        };
        fpopts.clip.onPause = function () {
            // log("flowplayer.onPause");
            media._trigger("pause");
        };
        fpopts.clip.onFinish = function () {
            // console.log("flowplayer.clip.onFinish");
            media._trigger("pause");
            media._trigger("ended");
        };
        fpopts.clip.onMetaData = function () {
            // log("flowplayer.clip.onMetaData");
            media._trigger("durationchange");
        }
        $(fpelt).flowplayer(fpswf, fpopts);
        that.fp = flowplayer(fpelt.get(0));

        that.play = function () {
            // log("flowplayer.play: loaded:", that.fp.isLoaded());
            if (!that.fp) { return; }
            var state = that.fp.getState();
            // log("play", state);
            /*
            http://flowplayer.org/documentation/api/player.html
            -1	unloaded
            0	loaded
            1	unstarted
            2	buffering
            3	playing
            4	paused
            5	ended
            */
            if (state == 4) {
                that.fp.resume();
                // log("flowplayer, resuming");
            } else if (state !== 3) {
                that.fp.play();
                // log("flowplayer, playing");
            }
            //else {
                // log("flowplayer, warning: bad playstate");
            // }
        };
        that.pause = function () {
            // log("flowplayer.pause");
            if (!that.fp) { return; }
            that.fp.pause();
        };
        that.getPaused = function () {
            if (!that.fp) return;
            return !that.fp.isPlaying();
        };
        that.getCurrentTime = function () {
            if (!that.fp) return;
            return that.fp.getTime();
        }
        that.seek = function (t) {
            // console.log("flowplayer seek", t, that.fp);
            var seek_result = that.fp.seek(t);
            // console.log("seek result:", seek_result);
        };
        that.getDuration = function () {
            // also a read-write value "duration" that allows clip to be stopped earlier
            if (!that.fp) return;
            var clip = that.fp.getClip();
            if (clip) return clip.fullDuration;
        }

        that.destroy = function () {
            that.fp = null;
            if (that.fpelt) that.fpelt.remove();
        }

        return that;
    };
    return klass;
})());


/**********************************/
/* HTML5 video tag delegate       */
/**********************************/

add_delegate((function () {
    var klass = {};
    klass.name = "html5";
    
    klass.sniff = function (url) {
        var urlpat = /og(v|g)$/i;
        return urlpat.test(url);
    };
    
    klass.init = function (media) {
        var o = media.options;
        var that = {name: klass.name};
        var videotag = null;

        // Init
        var tagstr = "<video";
        if (o.controls) { tagstr += " controls"; }
        if (o.autoplay) { tagstr += " autoplay"; }
        if (o.autobuffer) { tagstr += " autobuffer"; }
        tagstr += ">browser has no video tag support</video>";

        videotag = $(tagstr).attr("src", o.src).appendTo(media.element).get(0);
        /* Bind events */
        $(videotag).bind("play", media._trigger("play"));
        $(videotag).bind("pause", media._trigger("pause"));
        $(videotag).bind("ended", function () {
            /* addressing a bug that the video tag seems to receive an ended event
             * when a seek fails (seek beyond where it can read)
             * so suppress ended events when duration is undefined
             */
            if (that.getDuration()) media._trigger("ended");
        });

        // Export Required Methods
        that.play = function () { videotag.play(); }
        that.pause = function () { videotag.pause(); }
        that.getPaused = function () { return videotag.paused; };
        that.getCurrentTime = function () { return videotag.currentTime; }
        that.seek = function (t) {
            // videotag.currentTime = t;
            var tries = 0;
            function trytoseek () {
                try {
                    // log("trying to setCurrentTime", t);
                    videotag.currentTime = t;
                } catch (e) {
                    // log("retrying...", e);
                    tries += 1;
                    if (++tries >= 30*4) {
                        // log("setCurrentTime: giving up.");
                        return;
                    }
                    window.setTimeout(trytoseek, 250);
                }
            }
            trytoseek();
        };
        that.getDuration = function (t) {
            return videotag.duration;
        }

        that.destroy = function () {
            if (videotag) {
                $(videotag).remove();
                videotag = null;
            }
        }

        if (o._start) {
            that.seek(o._start);
        }

        return that;
    };
    
    return klass;

})());


/**********************************/
/* YouTube delegate               */
/**********************************/
// 
add_delegate((function () {
    var klass = {};
    klass.name = "youtube";

    // http://www.youtube.com/watch?v=fF7htGntFxA
    // http://www.youtube.com/user/TransforumNL#p/a/u/0/nDR_Vh4JsrA
    
    var urlpat = "youtube\\.com/.+[?=]v=([\\w-]+)";
    var urlpat2 = "youtube\\.com/.+/(\\w+)$";

    klass.sniff = function (url) {
        return new RegExp(urlpat).test(url) || new RegExp(urlpat2).test(url);
    };

    function id_from_url (url) {
        var results = new RegExp(urlpat).exec(url); //.match(urlpat);
        if (results) { return results[1]; }
        results = new RegExp(urlpat2).exec(url);
        if (results) { return results[1]; }
    }

    var aa_youtube_id = 0;
    
    klass.init = function (media) {
        var o = media.options;
        var that = {name: 'youtube'};
        that.youtube_id = id_from_url(o.src);
        // log("youtube_id", that.youtube_id, media.options.src);
        var chromeless = false;

        // INIT
        // clear previous
        that.o = o;
        // gotcha playerid can include URL encoding (because of swfobject?), avoiding "_" chars in ids!
        // actually observed different behavious between chromed + chromeless
        that.player_id = "aayoutube"+(++aa_youtube_id);
        aa_youtube_players_by_id[that.player_id] = that;
        that.player_id2 = that.player_id + "_embed";
        
        var newelt = $("<div></div>").appendTo(media.element);
        newelt.attr("id", that.player_id);

        var ytwidth = o.width || o.default_width;
        var ytheight = o.height || o.default_height;

        var params = { allowScriptAccess: "always", allowfullscreen: "true", wmode: "opaque" };
        var atts = { id: that.player_id2 };
        var swfurl;
        if (o.controls) {
            swfurl = "http://www.youtube.com/v/"+that.youtube_id+"?enablejsapi=1&playerapiid=" + that.player_id;
        } else {
            // chromeless player
            chromeless = true;
            swfurl = "http://www.youtube.com/apiplayer?enablejsapi=1&version=3&playerapiid=" + that.player_id;
        }
        swfobject.embedSWF(swfurl, that.player_id, ytwidth, ytheight, "8", null, null, params, atts);
        var player = document.getElementById(that.player_id2);
        log("got player", player);
        if (player) { setPlayer(player); }
        
        var playing = false;
        
        function setPlayer (p) {
            if (DEBUG) log("setPlayer", p);
            that.player = p;
            // forget about passing an anonymous function to youtube...
            // http://stackoverflow.com/questions/786380/using-youtubes-javascript-api-with-jquery
            // old-school ugly hack to the rescue!
            var callbackstr = '(function(state) { return onYouTubePlayerStateChange(state, "' + that.player_id + '"); })'
            var tries = 0;
            function tryto () {
                try {
                    p.addEventListener("onStateChange", callbackstr);
                } catch (e) {
                    if (DEBUG) log("tryto, retrying...");
                    tries += 1;
                    if (tries >= 30*4) {
                        if (DEBUG) log("tryto: giving up.");
                        return;
                    }
                    window.setTimeout(tryto, 250);
                }
            }
            tryto();
        }
        that.youtubeStateChange = function (newstate) {
            // log("onStateChange", newstate);
            // yt.getPlayerState: Possible values are unstarted (-1), ended (0), playing (1), paused (2), buffering (3), video cued (5).
            var now_playing = (newstate === 1);
            if (playing !== now_playing) {
                playing = now_playing;
                if (playing) {
                    media._trigger("play");
                } else {
                    media._trigger("pause");
                }
            }
            if (newstate == 0) {
                log("youtube statechange: clip ended");
                media._trigger("ended");
            }

        };
        that.youtubeReady = function () {
            if (!that.player) {
                var player = document.getElementById(that.player_id2);
                if (player) setPlayer(player);
            }
            // log("ready, player", that.player);
            if (chromeless) {
                if (that.o.autoplay) {
                    that.player.loadVideoById(that.youtube_id, o._start ? o._start : undefined);
                } else {
                    that.player.cueVideoById(that.youtube_id, o._start ? o._start : undefined);
                }
            } else {
                if (o._start) { that.player.seekTo(o._start, true); }
                if (that.o.autoplay) { that.player.playVideo(); } else { that.player.pauseVideo(); }
            }
        }
        that.play = function () {
            if (that.player) that.player.playVideo();
        }
        that.pause = function () {
            if (that.player) that.player.pauseVideo();
        }
        that.getPaused = function () {
            if (!that.player) return;
            // yt.getPlayerState: Possible values are unstarted (-1), ended (0), playing (1), paused (2), buffering (3), video cued (5).
            // try {
                return that.player.getPlayerState() !== 1;
            // } catch (e) {
            //    if (DEBUG) log("youtube.getPlayerState failed");
            // }
        };
        that.getCurrentTime = function () {
            try {
                return that.player.getCurrentTime();
            } catch (e) {
                if (DEBUG) log("youtube.getCurrentTime failed");
            }
        }
        that.seek = function (t) {
            // log("youtube.seek", t);
            if (that.player) that.player.seekTo(t, true);
        };
        that.getDuration = function () {
            if (!that.player) return;
            try {
                return that.player.getDuration();
            } catch (e) {
                if (DEBUG) log("youtube.getDuration failed");
            }
        };
        that.destroy = function () {
            delete aa_youtube_players_by_id[that.player_id];
            that.player = null;
            $(media.element).html("");
        }

        return that;
    };
    return klass;
})());

/**********************************/
/* Vimeo delegate                 */
/**********************************/
// 
add_delegate((function () {
    var klass = {};
    klass.name = "vimeo";

    // var urlpat= 'http://(?:www\\.)?vimeo\\.com/([\\d]+)';
    var urlpat= "(?:www\\.)?vimeo\\.com/([\\d]+)";

    klass.sniff = function (url) {
        return new RegExp(urlpat).test(url);
    };

    function id_from_url (url) {
        var results = new RegExp(urlpat).exec(url);
        // log("vimeo, id_from_url", results[1]);
        if (results) { return results[1]; }
    }

    var aa_vimeo_id = 0;
    
    klass.init = function (media) {
        var o = media.options;
        var that = {name: 'vimeo'};
        that.vimeo_id = id_from_url(o.src);
        // log("vimeo_id", that.vimeo_id);

        that.elt = $("<div></div>").appendTo(media.element);
        that.elt.addClass("aa_vimeo");
        that.player_id = o.vimeo_player_id || "aa_vimeo"+(++aa_vimeo_id);
        that.embed_id = that.player_id + "_embed";
        that.elt.attr("id", that.player_id);

        // eventually need to use these hooks...
        // js_onLoad: 'vimeo_player_loaded', 

        var flashvars = {
            clip_id: that.vimeo_id,
            show_portrait: 1,
            show_byline: 1,
            show_title: 1,
            js_api: 1, // required in order to use the Javascript API
            js_swf_id: 'moogaloop' // this will be passed into all event methods so you can keep track of multiple moogaloops (optional)
        };
        var params = {
            allowscriptaccess: 'always',
            allowfullscreen: 'true',
            wmode: 'opaque'
        };
        var attributes = {};

        swfobject.embedSWF("http://vimeo.com/moogaloop.swf", that.player_id, "504", "340", "9.0.0","expressInstall.swf", flashvars, params, attributes);
        that.player = null;
        that.player = document.getElementById(that.player_id);
        log("vimeo, player", that.player);
        
        var playing = false;
        
        function setPlayer (p) {
            if (DEBUG) log("setPlayer", p);
            that.player = p;
        }
        that.play = function () {
            if (that.player) {
                that.player.api_play();
                // todo get this from the player
                playing = true;
            }
        }
        that.pause = function () {
            if (that.player) {
                that.player.api_pause();
                // todo get this from the player
                playing = false;
            }
        }
        that.getPaused = function () {
            if (!that.player) return;
            return !playing;
        };
        that.getCurrentTime = function () {
            if (!that.player) return;
            try {
                return that.player.api_getCurrentTime();
            } catch (e) {
                if (DEBUG) log("vimeo.getCurrentTime failed");
            }
        }
        that.seek = function (t) {
            // log("vimeo.seek", t);
            var tries = 0;
            function trytoseek () {
                try {
                    // log("vimeo.seek: trying", t);
                    that.player.api_seekTo(t);
                } catch (e) {
                    // log("vimeo.seek, retrying...");
                    tries += 1;
                    if (++tries >= 30*4) {
                        // log("vimeo.seek: giving up.");
                        return;
                    }
                    window.setTimeout(trytoseek, 250);
                }
            }
            trytoseek();
        };
        that.getDuration = function () {
            if (!that.player) return;
            try {
                return that.player.api_getDuration();
            } catch (e) {
                if (DEBUG) log("vimeo.getDuration failed");
            }
        };
        that.destroy = function () {
            // log("vimeo: unload");
            try {
                if (that.player) that.player.api_unload();
            } catch (e) {
                if (DEBUG) log("vimeo.unload failed");
            }
            that.player = null;
            // not sure why this doesn't work ??
            // that.elt.remove();
            $(media.element).html("");
        }

        if (o._start) {
            that.seek(o._start);
        }

        return that;
    };
    return klass;
})());

/**********************************/
/* TOTEM plugin delegate          */
/**********************************/

add_delegate((function () {
    var klass = {};
    klass.name = "totem";
    
    klass.sniff = function (url) {
        var urlpat = /(dv|avi|mov)$/i;
        return urlpat.test(url);
    };
    
    klass.init = function (media) {
        var o = media.options;
        var that = {name: klass.name};
        var videotag = null;

        // Init
        var tagstr = "<embed controls='no' type='application/x-totem-plugin'";
        // if (o.controls) { tagstr += " controls"; }
        tagstr += o.autoplay ? " autoplay='yes'" : " autoplay='no'";
        // if (o.autobuffer) { tagstr += " autobuffer"; }
        tagstr += "></embed>";

        // also evt. interesting to control / use: vlc.audio.track [0-65535]

        videotag = $(tagstr).attr("src", o.src).appendTo(media.element).get(0);
        // console.log("src", o.src);

        var w = o.width || o.default_width;
        var h = o.height || (o.audio ? o.default_audio_height : o.default_height);
        $(videotag).css({
            width: w + "px",
            height: h + "px"
        });

        /* Bind events */
        // $(videotag).bind("play", media._trigger("play"));
        // $(videotag).bind("pause", media._trigger("pause"));
        // $(videotag).bind("ended", function () {
            /* addressing a bug that the video tag seems to receive an ended event
             * when a seek fails (seek beyond where it can read)
             * so suppress ended events when duration is undefined
             */
            // if (that.getDuration()) media._trigger("ended");
        // });

        // Export Required Methods
        that.play = function () { 
            if (!videotag.playlist.isPlaying) { videotag.playlist.togglePause(); }
        }
        that.pause = function () {
            if (videotag.playlist.isPlaying) { videotag.playlist.togglePause(); }
        }
        that.getPaused = function () { return !videotag.playlist.isPlaying; };
        that.getCurrentTime = function () { return (videotag.input.time / 1000); }
        that.seek = function (t) {
            videotag.input.time = (t*1000);
        };
        that.getDuration = function (t) {
            return (videotag.input.length / 1000);
        }

        that.destroy = function () {
            if (videotag) {
                $(videotag).remove();
                videotag = null;
            }
        }

        if (o._start) {
            that.seek(o._start);
        }

        return that;
    };
    
    return klass;

})());


})(jQuery);

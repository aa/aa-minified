<?php
class timecrumb{
  private $start_time=null;
  private $end_time=null;
  private $source='';
  private $markup='';
  private $resource=null;
  private $annotation=null;
  private $id=null;

  function __construct($id){
    $this->id=$id;
}
  public static function getInstance($serializedobject){
    //factory
    //sets properties according to $serializedobject
}
  public function load(){
    //read from whatever serialized version
    //not to be used beside for testing purpose since the timecrumb object only exist for reordering and consistency checks of the annotation
}
  public function save(){
    //save to whatever serialized version
    //not to be used beside for testing purpose since the timecrumb object only exist for reordering and consistency checks of the annotation

    ser_save($this->id,serialize($this),'persist');
}

  public function set_start_time($start_time){
    $this->start_time=$start_time;

}
  public function get_start_time(){
    return $this->start_time;

}
  public function set_end_time($end_time){
    $this->end_time=$end_time;

}
  public function get_end_time(){
    return $this->end_time;

}
  public function set_annotation($annotation){
    $this->annotation=$annotation;

}
  public function get_annotation(){
    return $this->annotation;

}  
  public function set_resource($resource){
    $this->resource=$resource;

}
  public function get_resource(){
    return $this->resource;

}
  public function set_source($source){
    $this->source=$source;

}
  public function get_source(){
    return $this->source;

}
  public function set_markup(){
    //etrange textile_to_html n'est pas definie mais ne produit pas d'erreur ...
    //$this->markup=textile_to_html($this->source);

}
  public function get_markup(){
    return $this->markup;

}
}

?>
/**
 * @fileoverview this file provides $.delayedaction, a jQuery plugin that
 * enables a function call to be scheduled, and optionally cancelled,
 * before occurring after some time.
 * @author Michael Murtaugh <mm@automatist.org> and the Active Archives contributors
 * @license GNU AGPL
 */


(function ($) {

/**
 * Delays a function passed as an argument
 * @private
 * @param {Function} action  
 * @param {Integer} delay in milliseconds
 */
function delayedaction (action, time) {
    if (time === undefined) { time = 1000; }
    var timeout_id = null;
    var that = {};
    function cancel () {
        if (timeout_id !== null) {
            window.clearTimeout(timeout_id);
            timeout_id = null;
        }
    }
    that.cancel = cancel;
    function performAction () {
        action();
        timeout_id = null;
    }
    that.do_soon = function () {
        cancel();
        timeout_id = window.setTimeout(performAction, time);
    };
    that.do_now = function () {
        performAction();
    };
    return that;
}

/**
 * Delays a function passed as an argument
 * @private
 * @param {Function} action  
 * @param {Integer} delay in milliseconds
 */
$.delayedaction = delayedaction;

})(jQuery);

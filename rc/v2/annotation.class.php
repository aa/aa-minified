<?php
class annotation{
  /* var $start_time=null; */
  /* var $end_time=null; */

  private $id, $json_source, $resource, $source, $left, $top, $width, $height, $section_ids, $annotation_id,$timecrumbs,$editor_header,$annotation_header,$versions;
 

  function __construct($id){
    $this->id=$id;
}
  public static function getInstance($serializedobject){
    //factory
    //sets properties according to $serializedobject
}
  public function load(){
    //read from whatever serialized version

}
  public function load_from_json($is_version=false){
    if($is_version){
      $load_path=VERSIONPATH;
    }else{
      $load_path=SERPATH;
    }
    $json_seed=json_decode(unserialize(ser_load($this->id,$load_path,'.txt')));
    $this->resource=$json_seed->resource;
    $this->source=$json_seed->source;
    $this->left=$json_seed->left;
    $this->top=$json_seed->top;
    $this->width=$json_seed->width;
    $this->height=$json_seed->height;
    $this->section_ids=$json_seed->section_ids;
    $this->annotation_id=$json_seed->annotation_id;
    $this->json_source=$json_seed;
  }
  public function save(){
    //save to whatever serialized version
    ser_save($this->id,serialize($this),'persist/ser');
}
 
  public function set_resource($resource){
    $this->resource=$resource;

}
  public function get_resource(){
    return $this->resource;

}
  public function set_json_source($json_source){
    $this->json_source=$json_source;

}
  public function get_json_source(){
    return $this->json_source;

}
  public function set_source($source){
    $this->source=$source;

}
  public function get_source(){
    return $this->source;

}
  public function export_json(){
    //save to whatever serialized version
    ser_save($this->id,serialize($this->json_source),SERPATH,'.txt',true);
    $version_suffix=preg_replace("/(\.|\s)/","",microtime());
    $version_name=$this->id.'__'.$version_suffix;
    ser_save($version_name,serialize($this->json_source),VERSIONPATH,'.txt',true);

}
  public function get_timecrumbs(){
    return $this->timecrumbs;
}
  public function set_timecrumbs($timecrumbs){
    $this->timecrumbs=$timecrumbs;
}
  public function set_annotation_header($annotation_header){
    $this->annotation_header=$annotation_header;
}
  public function set_editor_header(){
    $this->editor_header='<div class="aa-playlist" data-resource="'.$this->resource.'" data-id="'.$this->id.'" style="top: '.$this->top.'px; left: '.$this->left.'px; width: '.$this->width.'px; height: '.$this->height.'px">'."\n";
}
  public function generate_editor_html(){
    $this->set_editor_header();
    $editor_html=$this->editor_header;
    $i=0;
    if(strlen($this->annotation_header)>0){
      $editor_html.='<div class="aa-playlist-section" data-id="annotation_header" data-url="'.$this->resource.'" ><p>'.$this->annotation_header.'</p>';
     $editor_html.='<textarea class="aa-playlist-section-source">'.$this->annotation_header."</textarea>\n";
      $editor_html.="</div>\n";//closing annotation_header
}
    foreach($this->timecrumbs as $crumb){
      $editor_html.='<div class="aa-playlist-section" data-id="'.$i.'" data-url="'.$this->resource.'" ';
      if(strlen($crumb->get_end_time())>0){
	$editor_html.='data-start="'.tc_to_milli($crumb->get_start_time()).'" data-end="'.tc_to_milli($crumb->get_end_time()).'">'."\n";
      }else{
	$editor_html.='data-start="'.tc_to_milli($crumb->get_start_time()).'">'."\n";
      }
      $lines=preg_split('/\n/',$crumb->get_source());
      foreach($lines as $line){
      $editor_html.='<p>'.$line."</p>\n";
      }
      $editor_html.='<textarea class="aa-playlist-section-source">'.$crumb->get_source()."</textarea>\n";
      $editor_html.="</div>\n";//closing section
      $i++;
    }
    $editor_html.="</div>\n";//closing header
    return $editor_html;
  }
  public function generate_video_widget_html($limit=1){
    $i=0;
    $widget_html.='';
    foreach($this->timecrumbs as $crumb){
      if($i<$limit){
      $widget_html.='<div class="widget_video_env">';
      $widget_html.='<div class="screen"><video class="myvideo_" id="vid_'.$i.'" width="300px" height="225px" src="'.$this->resource.'"></video></div>';
	$widget_html.='<div class="crumb_element" data-id="'.$i.'" data-url="'.$this->resource.'" ';
	if(strlen($crumb->get_end_time())>0){
	  $widget_html.='data-start="'.tc_to_milli($crumb->get_start_time()).'" data-end="'.tc_to_milli($crumb->get_end_time()).'">'."\n";
	}else{
	  $widget_html.='data-start="'.tc_to_milli($crumb->get_start_time()).'">'."\n";
	}
	$widget_html.='<div class="carton">';
	$lines=preg_split('/\n/',$crumb->get_source());
	foreach($lines as $line){
	  $widget_html.='<p>'.$line."</p>\n";
	}
	$widget_html.="</div>\n";//closing carton
	$widget_html.="</div>\n";//closing section
      $widget_html.="</div>\n";//closing widget env

      }

      $i++;
    }

    return $widget_html;
  }
  public function generate_json_base($str_resource){
    $ar_json=array();
    $ar_json['resource']=$str_resource;
    $ar_json['source']="00:00,000 -->\nStart here\n";
    $ar_json['left']='722';
    $ar_json['top']='47';
    $ar_json['width']='240';
    $ar_json['height']='240';
    $ar_json['section_ids']='0';
    $ar_json['annotation_id']=preg_replace("/(\.|\s)/","",microtime());
    $str_json=serialize(json_encode($ar_json));
    ser_save($ar_json['annotation_id'],$str_json,SERPATH,'.txt',true);
    ser_save($ar_json['annotation_id'],$str_json,VERSIONPATH,'.txt',true);
    return $ar_json['annotation_id'];
  }

  public function get_versions(){
    return $this->versions;
}
  public function set_versions(){
    $ar_list=array();
    $ar_versions=array();
    if ($handle = opendir(VERSIONPATH)) {
      while (false !== ($entry = readdir($handle))) {
	if ($entry != "." && $entry != "..") {
	  $pieces=explode("__",$entry);
	  if($pieces[0]==$this->id){
	    $ar_versions[]=VERSIONPATH.'/'.$entry;
	  }
	}}
    }
    closedir($handle);
    asort($ar_versions);
    $this->versions=$ar_versions;
  }

  function revert($to){
    /* ser_save($this->id,serialize($this->json_source),SERPATH,'.txt',true); */

}
}
?>
(function ($, undefined) {

function log () {
    try { console.log.apply(console, arguments); } catch (e) {}
}

$.animatedsetter = function (opts) {
    // requires getter, setter functions
    
    var o = $.extend({}, $.animatedsetter.defaults, opts);
    var that = {};
    var dest;
    var intervalid = null;
    var lastvalue;
    
    that.set = function (val) {
        dest = val;
        if (intervalid === null) intervalid = window.setInterval(hook, o.intervaltime);
    }

    function hook () {
        // log("animatedsetter.hook");
        var cur = o.getter(); // that.element.scrollTop();
        var delta = (dest - cur); //  (o.animScrollDest - cur);
        // log("*", cur, delta, smax);
        var done = false;
        if (Math.abs(delta) < o.mindelta) {
            done = true;
        } else {
            delta *= o.speedfactor;
            var newvalue = Math.floor(cur + delta);
            done = o.nochangeautostop && (newvalue === lastvalue); // STOPS AUTOMATICALLY WHEN VALUE STOPS CHANGING!
        }
        if (done) {
            // log("animatedsetter.hook: done");
            o.setter(dest); // that.element.scrollTop(o.animScrollDest);
            window.clearInterval(intervalid);
            intervalid = null;
            // ALLOW FOR simple onend CALLBACK
            if (o.end) o.end(); // o.animScrollingReseter.do_soon();
        } else {
            // log("scrollTop", newvalue);
            o.setter(newvalue); // that.element.scrollTop(newvalue);
            lastvalue = newvalue;
        }
    }
    
    return that;
}
$.animatedsetter.defaults = {
    intervaltime: 50,
    mindelta: 1,
    speedfactor: 0.1,
    end: null,
    nochangeautostop: true
}

})(jQuery);

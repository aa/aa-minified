(function ($) {

/* Requires: jquery.timecode.js */
/*
Change log
==========

14 Jul 2010:
pulling into aa/browser code

17 May 2010:
separated from titles code (again)

$.timeline

Timeline organizes HTML by time.
Elements are attached to a timeline with a start time and (optionally) an end time.
A Timeline has a notion of a currentTime, and manages hiding / showing elements accordingly.
A Timeline is passive in that it requires an external element (such as a video element and/or a scrubber/controller) to "drive" it.

*/

var aa_timeline = function (options) {
    var that = {};
    var cc_item_uid = 0;

    var settings = {};
    $.extend(settings, options);

    // element wrapper
    var timeline_item = function (elt, start, end) {
        var that = {};
        cc_item_uid += 1;
        that.id = "T" + cc_item_uid;
        that.start = start;
        that.end = end;
        that.elt = elt;
        return that;
    };

    var paused = true;
    var currentTime = 0.0;

    var titlesByStart = [];
    var titlesByEnd = [];
    var lastTime = undefined;
    var startIndex = -1;
    var endIndex = -1;
    var toShow = {};
    var toHide = {};
    var activeItems = {};
    
    function addTitle (newtitle) {
        // addTitleByStart
        /* insert annotation in the correct (sorted) location */
        var placed = false;
        for (var i=0; i<titlesByStart.length; i++) {
            if (titlesByStart[i].start > newtitle.start) {
                // insert before this index
                titlesByStart.splice(i, 0, newtitle);
                placed = true;
                break;
            }
        }
        // otherwise simply append
        if (!placed) { titlesByStart.push(newtitle); }

        // addTitleByEnd
        /* insert annotation in the correct (sorted) location */
        placed = false;
        for (i=0; i<titlesByEnd.length; i++) {
            if ((titlesByEnd[i].end > newtitle.end) || (titlesByEnd[i].end === undefined && newtitle.end !== undefined)) {
                // insert before this index
                titlesByEnd.splice(i, 0, newtitle);
                placed = true;
                return;
            }
        }
        // otherwise simply append
        if (!placed) { titlesByEnd.push(newtitle); }
    }
    
    function markToShow (t) {
        if (toHide[t.id]) {
            delete toHide[t.id];
        } else {
            toShow[t.id] = t;
        }
    }

    function markToHide (t) {
        if (toShow[t.id]) {
            delete toShow[t.id];
        } else {
            toHide[t.id] = t;
        }
    }
    
    function show (elt) {
        // $(elt).trigger("show");
        if (settings.show) { settings.show(elt); }
    }
    function hide (elt) {
        if (settings.hide) { settings.hide(elt); }
    }
        
    function updateForTime (time) {
        if (titlesByStart.length === 0) { return; }
        var n;
        /* check against lastTime to optimize search */
        // valid range for i: -1            (pre first title)
        //                 to  titles.length-1 (last title), can't be bigger, as this isn't defined (when would it go last -> post-last)
        if (time < lastTime) {
            // SLIDE BACKWARD
            //
            n = titlesByStart.length;
            // [start: 50, start: 70]
            // [end: 55, end: 75]
            // time = 80
            // startIndex = 1 (at end)
            // process ends first! (as shows of same element will override!! when going backwards, dus)
            while (endIndex >= 0 && time < titlesByEnd[endIndex].end) {
                markToShow(titlesByEnd[endIndex]);
                endIndex--;
            }
            while (startIndex >= 0 && time < titlesByStart[startIndex].start) {
                markToHide(titlesByStart[startIndex]);
                startIndex--;
            }
        } else {
            // SLIDE FORWARD
            // 
            // process starts first! (as hides of same element will override!!)
            n = titlesByStart.length;
            while ((startIndex+1) < n && time >= titlesByStart[startIndex+1].start) {
                startIndex++;
                if (startIndex < n) { markToShow(titlesByStart[startIndex]); }
            }    
            n = titlesByEnd.length;
            while ((endIndex+1) < n && time >= titlesByEnd[endIndex+1].end) {
                endIndex++;
                if (endIndex < n) { markToHide(titlesByEnd[endIndex]); }
            }    
        }
        // if (this.startIndex != si) this.setStartIndex(si);
        lastTime = time;

        // perform show/hides
        var clearFlag = false;
        var tid;
        for (tid in toShow) {
            if (toShow.hasOwnProperty(tid)) { // JSLint (not strictly necessary)
                show(toShow[tid].elt);
                activeItems[tid] = toShow[tid];
                clearFlag = true;
            }
        }
        if (clearFlag) { toShow = {}; }
        clearFlag = false;
        for (tid in toHide) {
            if (toHide.hasOwnProperty(tid)) { // JSLint (not strictly necessary)
                hide(toHide[tid].elt);
                delete activeItems[tid];
                clearFlag = true;
            }
        }
        if (clearFlag) { toHide = {}; }

        return;
    }

    function add (thing, start, end) {
        if (typeof(start) == "string") {
            start = $.timecode_tosecs(start);
        }
        if (typeof(end) == "string") {
            end = $.timecode_tosecs(end);
        }

        // do some sanity checking
        if (start === undefined) { return "no start time"; }
        if (end && (end < start)) { return "end is before start"; }

        // wrap & add elt
        var item = timeline_item(thing, start, end);
        addTitle(item);

        // show/hide as appropriate, add to activeItems if needed
        if (currentTime >= start && (end === undefined || currentTime < end)) {
            show(item.elt);
            activeItems[item.id] = item;
        } else {
            hide(item.elt);
        }
        setCurrentTime(currentTime);
    }
    that.add = add;

    function setCurrentTime (ct, controller) {
        currentTime = ct;
        updateForTime(ct);
//        for (var id in activeItems) {
//            var item = activeItems[id];
//            item.item.setCurrentTime(ct - item.start);
//        }
    }
    that.setCurrentTime = setCurrentTime;

    that.getCurrentTime = function () { return currentTime; };
    
//    function debug () {
//        $.log("titlesByStart");
//        for (var i=0; i<titlesByStart.length; i++) {
//            var t = titlesByStart[i];
//            $.log("    ", t.elt, t.start, "("+t.end+")");
//        }
//        $.log("titlesByEnd");
//        for (var i=0; i<titlesByEnd.length; i++) {
//            var t = titlesByEnd[i];
//            $.log("    ", t.elt, t.end, "("+t.start+")");
//        }
//    }
//    that.debug = debug;
    return that;
};

$.aa_timeline = aa_timeline;
$.timeline = aa_timeline;

})(jQuery);


